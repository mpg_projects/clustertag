package no.simula.clustertag;

import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import org.newdawn.slick.*;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.imageout.ImageOut;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.DataBufferInt;
import java.awt.image.Raster;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Engine extends BasicGame {
    private final double eps = 1e-6;
    private final int thumbSize = 64;

    private Tree<Frame> tree;
    private final Object treeLock = new Object();
    private String rootDirPath = "";

    private boolean paintFirst = true;
    private boolean fitView = false;
    private boolean repaint = false;
    private boolean drawPos = false;
    private boolean loadThumbs = false;
    private boolean thumbsLoading = false;

    private double viewX = 0.0;
    private double viewY = 0.0;
    private double viewScale = 1.0;
    private int viewW = 0;
    private int viewH = 0;

    private boolean mouseLeftPressed = false;
    private int mouseX = 0;
    private int mouseY = 0;

    ArrayList<PictureInternal> picturesToProces = new ArrayList<PictureInternal>();
    ArrayList<PictureInternal> picturesReady = new ArrayList<PictureInternal>();
    private final Object picturesLock = new Object();

    ArrayList<ImageInternal> imagesToProces = new ArrayList<ImageInternal>();
    ArrayList<ImageInternal> imagesReady = new ArrayList<ImageInternal>();
    private final Object imagesLock = new Object();

    private RandomAccessFile thumbFile = null;
    private long thumbFileReaded = 0;
    private long thumbFileReadedOk = 0;
    private long thumbFileLength = 0;

    HashMap<String,Thumb> thumbs = new HashMap<String,Thumb>();

    public Engine(String gamename)
    {
        super(gamename);
    }

    @Override
    public void mouseWheelMoved(int change) {
        double x = (mouseX - viewW / 2.0) / viewScale + viewX;
        double y = (mouseY - viewH / 2.0) / viewScale + viewY;
        viewScale += viewScale * 0.001 * (double) change;
        if (viewScale < eps) {
            viewScale = eps;
        }
        viewX = x - (mouseX - viewW / 2.0) / viewScale;
        viewY = y - (mouseY - viewH / 2.0) / viewScale;
        drawPos = true;
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        if (button == 0) {
            mouseLeftPressed = true;
        }
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        if (button == 0) {
            mouseLeftPressed = false;
        }
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        mouseX = newx;
        mouseY = newy;
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        if (mouseLeftPressed) {
            viewX += (oldx - newx) / viewScale;
            viewY += (oldy - newy) / viewScale;
            drawPos = true;
        }
        mouseX = newx;
        mouseY = newy;
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
        int cores = Runtime.getRuntime().availableProcessors();
        for (int threadIdx = 0; threadIdx < cores; threadIdx ++) {
            new Thread() {
                public void run() {
                    try {
                        while (true) {
                            Thread.sleep(1);
                            PictureInternal pict = null;
                            synchronized (picturesLock) {
                                if (picturesToProces.size() > 0) {
                                    pict = picturesToProces.get(0);
                                    picturesToProces.remove(0);
                                }
                            }
                            if (pict != null) {
                                BufferedImage img = null;
                                try {
                                    img = ImageIO.read(new File(pict.getImagePath()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    System.out.println(e);
                                }
                                if (img != null) {
                                    double iw = img.getWidth();
                                    double ih = img.getHeight();
                                    if (iw > 0.0 && ih > 0.0) {
                                        pict.setRatio(iw / ih);
                                        int thumbW = (int) (pict.getRatio() >= 1.0 ? thumbSize : (int) Math.round(Math.floor(thumbSize * pict.getRatio())));
                                        int thumbH = (int) (pict.getRatio() >= 1.0 ? (int) Math.round(Math.floor(thumbSize / pict.getRatio())) : thumbSize);
                                        if (thumbW > 0 && thumbH > 0) {
                                            BufferedImage thumbImage = new BufferedImage(thumbW, thumbH, BufferedImage.TYPE_INT_RGB);
                                            Graphics2D gr = thumbImage.createGraphics();
                                            //tGraphics2D.setBackground(Color.WHITE);
                                            //tGraphics2D.setPaint( Color.WHITE );
                                            //tGraphics2D.fillRect( 0, 0, tThumbWidth, tThumbHeight );
                                            gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                                            gr.drawImage(img, 0, 0, thumbW, thumbH, null);
                                            gr.dispose();
                                            ByteArrayOutputStream thumbBB = new ByteArrayOutputStream(0);
                                            try {
                                                ImageIO.write(thumbImage, "PNG", thumbBB);
                                            } catch (IOException e) {
                                                e.printStackTrace();
                                                System.out.println(e);
                                            }
                                            if (thumbBB.size() > 0) {
                                                pict.setThumbData(thumbBB.toByteArray());
                                                thumbBB = null;
                                                double thumbC[] = {0.0, 0.0, 0.0, 0.0};
                                                for (int y = thumbImage.getHeight() / 5; y < thumbImage.getHeight() * 4 / 5; y++) {
                                                    for (int x = thumbImage.getWidth() / 5; x < thumbImage.getWidth() * 4 / 5; x++) {
                                                        Color c = new Color(thumbImage.getRGB(x, y));
                                                        thumbC[0] += c.getRed();
                                                        thumbC[1] += c.getGreen();
                                                        thumbC[2] += c.getBlue();
                                                        thumbC[3] += 1.0;
                                                    }
                                                }
                                                int thumbCI[] = {0, 0, 0};
                                                if (thumbC[3] > eps) {
                                                    for (int _i = 0; _i < 3; _i++) {
                                                        int c = (int) (Math.round(Math.floor(thumbC[_i] / thumbC[3])));
                                                        if (c < 0) {
                                                            c = 0;
                                                        }
                                                        if (c > 255) {
                                                            c = 255;
                                                        }
                                                        thumbCI[_i] = c;
                                                    }
                                                }
                                                pict.setColor(((thumbCI[0]) << 16) | ((thumbCI[1]) << 8) | (thumbCI[2]));
                                                for (String gtClassName : pict.getImageGTPaths().keySet()) {
                                                    int objectClassId = Objects.getObjectClassId(gtClassName);
                                                    if (objectClassId > 0) {
                                                        for (String gtPath : pict.getImageGTPaths().get(gtClassName)) {
                                                            BufferedImage gtImg = null;
                                                            try {
                                                                gtImg = ImageIO.read(new File(gtPath));
                                                            } catch (IOException e) {
                                                                e.printStackTrace();
                                                                System.out.println(e);
                                                            }
                                                            try {
                                                                if (img.getWidth() != gtImg.getWidth() || img.getHeight() != gtImg.getHeight()) {
                                                                    throw new Exception("Image dimensions mismatch corresponding ground truth image");
                                                                }
                                                            } catch (Exception e) {
                                                                e.printStackTrace();
                                                                System.out.println(e);
                                                                gtImg = null;
                                                            }
                                                            if (gtImg == null) {
                                                                continue;
                                                            }
                                                            int w = img.getWidth();
                                                            int h = img.getHeight();
                                                            BufferedImage image_rgb = new BufferedImage(w, h, BufferedImage.TYPE_INT_BGR);
                                                            image_rgb.getGraphics().drawImage(gtImg, 0, 0, null);
                                                            int[] pixels = ((DataBufferInt) image_rgb.getRaster().getDataBuffer()).getData();
                                                            int[][] grayscale = new int[w][h];
                                                            for (int x = 0; x < w; x++) {
                                                                for (int y = 0; y < h; y++) {
                                                                    int pixel = pixels[y * w + x];
                                                                    int b = (pixel >> 16) & 0xFF;
                                                                    int g = (pixel >> 8) & 0xFF;
                                                                    int r = (pixel) & 0xFF;
                                                                    grayscale[x][y] = (r >= 128 && g >= 128 && b >= 128) ? -1 : 0;
                                                                }
                                                            }
                                                            image_rgb = null;
                                                            pixels = null;
                                                            int regionCount = 0;
                                                            boolean lookAgain = true;
                                                            while (lookAgain) {
                                                                lookAgain = false;
                                                                boolean newRegion = true;
                                                                boolean lookingRegion = true;
                                                                while (lookingRegion) {
                                                                    lookingRegion = false;
                                                                    for (int i = 0; i < w; i++) {
                                                                        for (int j = 0; j < h; j++) {
                                                                            if (grayscale[i][j] < 0 && (((i > 0 && grayscale[i - 1][j] == regionCount) ||
                                                                                    (i < w - 1 && grayscale[i + 1][j] == regionCount) ||
                                                                                    (j > 0 && grayscale[i][j - 1] == regionCount) ||
                                                                                    (j < h - 1 && grayscale[i][j + 1] == regionCount)) ||
                                                                                    (!lookingRegion))) {
                                                                                if (newRegion) {
                                                                                    regionCount++;
                                                                                    newRegion = false;
                                                                                }
                                                                                lookingRegion = true;
                                                                                lookAgain = true;
                                                                                grayscale[i][j] = regionCount;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            for (int regionInd = 0; regionInd < regionCount; regionInd++) {
                                                                int xMin = -1;
                                                                int yMin = -1;
                                                                int xMax = -1;
                                                                int yMax = -1;
                                                                for (int i = 0; i < w; i++) {
                                                                    for (int j = 0; j < h; j++) {
                                                                        if (grayscale[i][j] == regionInd + 1) {
                                                                            if (xMin > i || xMin < 0) {
                                                                                xMin = i;
                                                                            }
                                                                            if (xMax < i || xMax < 0) {
                                                                                xMax = i;
                                                                            }
                                                                            if (yMin > j || yMin < 0) {
                                                                                yMin = j;
                                                                            }
                                                                            if (yMax < j || yMax < 0) {
                                                                                yMax = j;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                pict.getObjects().addObject(objectClassId, new ObjectPos(1.0, new Rectangle2D.Double((double) xMin / (double) w, (double) yMin / (double) h, (double) (xMax - xMin + 1) / (double) w, (double) (yMax - yMin + 1) / (double) h)));
                                                            }
                                                            grayscale = null;
                                                        }
                                                    }
                                                }
                                                for (int featureIdx = 0; featureIdx < pict.getFeatures().getGlobalFeaturesNum(); featureIdx++) {
                                                    GlobalFeature featureExtractor = pict.getFeatures().getGlobalFeatureExtractorInstance(featureIdx);
                                                    featureExtractor.extract(img);
                                                    pict.getFeatures().setGlobalFeature(featureIdx, double2float(featureExtractor.getFeatureVector()));
                                                }
                                                synchronized (picturesLock) {
                                                    picturesReady.add(pict);
                                                }
                                            }
                                            thumbImage = null;
                                        }
                                    }
                                    img = null;
                                }
                                Thread.sleep(25);
                                continue;
                            }
                            ImageInternal imageInternal = null;
                            synchronized (imagesLock) {
                                if (imagesToProces.size() > 0) {
                                    imageInternal = imagesToProces.get(0);
                                    imagesToProces.remove(0);
                                }
                            }
                            if (imageInternal != null) {
                                BufferedImage img = null;
                                try {
                                    img = ImageIO.read(new File(imageInternal.getImagePath()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    System.out.println(e);
                                }
                                if (img != null) {
                                    if (img.getWidth() > 0 && img.getHeight() > 0) {
                                        ByteArrayOutputStream imageBB = new ByteArrayOutputStream(0);
                                        try {
                                            ImageIO.write(img, "PNG", imageBB);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                            System.out.println(e);
                                        }
                                        if (imageBB.size() > 0) {
                                            imageInternal.setImageData(imageBB.toByteArray());
                                            imageBB = null;
                                            synchronized (imagesLock) {
                                                imagesReady.add(imageInternal);
                                            }
                                        }
                                    }
                                    img = null;
                                }
                                Thread.sleep(25);
                                continue;
                            }
                            Thread.sleep(100);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        System.out.println(e);
                    }
                }
            }.start();
        }
    }

    @Override
    public void update(GameContainer gc, int i) throws SlickException {
        if (tree == null) {
            return;
        }
        if (paintFirst) {
            paintFirst = false;
            return;
        }
        synchronized (treeLock) {
            viewW = gc.getWidth();
            viewH = gc.getHeight();
            ArrayList<TreeNode<Frame>> clusterNodes = new ArrayList<TreeNode<Frame>>();
            tree.walk(true, new TreeProcessor<Frame>() {
                @Override
                public void processNode(TreeNode<Frame> node) {
                    Frame frame = node.getData();
                    if (!frame.isLeaf()) {
                        clusterNodes.add(node);
                    }
                }
            });
            boolean repos = false;
            for (TreeNode<Frame> clusterNode : clusterNodes) {
                Frame clusterFrame = clusterNode.getData();
                boolean clusterRepos = false;
                for (TreeNode<Frame> node : clusterNode.getChildren()) {
                    Frame frame = node.getData();
                    if (!frame.getRPos()) {
                        clusterRepos = true;
                    }
                }
                if (clusterRepos) {
                    double ROffs = 0.0;
                    for (int circleIdx = 0; circleIdx < 3; circleIdx++) {
                        ArrayList<TreeNode<Frame>> circleNodes = new ArrayList<TreeNode<Frame>>();
                        for (TreeNode<Frame> node : clusterNode.getChildren()) {
                            Frame frame = node.getData();
                            if (!frame.getRPos()) {
                                if ((circleIdx == 0 && frame.getD() == 0.0) || (circleIdx == 1 && frame.getD() > 0.0) || (circleIdx == 2 && frame.getD() < 0.0)) {
                                    circleNodes.add(node);
                                }
                            }
                        }
                        if (circleNodes.size() == 0) {
                            continue;
                        }
                        if (circleIdx == 1) {
                            Collections.sort(circleNodes, new Comparator<TreeNode<Frame>>() {
                                public int compare(TreeNode<Frame> node1, TreeNode<Frame> node2) {
                                    double d1 = node1.getData().getD();
                                    double d2 = node2.getData().getD();
                                    return (d1 == d2) ? 0 : (d1 < d2) ? -1 : 1;
                                }
                            });
                        }
                        double r = 0.0;
                        for (TreeNode<Frame> node : circleNodes) {
                            Frame frame = node.getData();
                            if (r < frame.getR()) {
                                r = frame.getR();
                            }
                        }
                        double posR = (ROffs == 0.0) ? 0.0 : (ROffs + r * 2.0);
                        long posNum = 1;
                        long posLeft = 1;
                        long nodesLeft = circleNodes.size();
                        for (TreeNode<Frame> node : circleNodes) {
                            Frame frame = node.getData();
                            if (posLeft == 0) {
                                posR += r * 2.0;
                                double l = posR * Math.PI * 2.0;
                                posNum = Math.round(Math.floor(l / (r * 2.0)));
                                if (posNum > nodesLeft) {
                                    posNum = nodesLeft;
                                }
                                posLeft = posNum;
                            }
                            double a = Math.PI * 2.0 * (double) (posNum - posLeft) / (double) (posNum);
                            double rr = posR;// : (posR + r * 2.0) * (double) (posNum - posLeft) / (double) (posNum) + (posR) * (1.0 - (double) (posNum - posLeft) / (double) (posNum));
                            frame.setRX(rr * Math.cos(a));
                            frame.setRY(- rr * Math.sin(a));
                            frame.setRPos(true);
                            repos = true;
                            posLeft--;
                            nodesLeft--;
                        }
                        ROffs = posR;
                        clusterFrame.setR(ROffs + r * 2.0);
                    }
                }
            }
            if (loadThumbs) {
                try {
                    thumbFile = new RandomAccessFile(rootDirPath + File.separator + ".thumb.db", "rw");
                    thumbFileReaded = 0;
                    thumbFileReadedOk = 0;
                    thumbFileLength = thumbFile.length();
                    thumbsLoading = true;
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                loadThumbs = false;
            }
            if (thumbsLoading) {
                boolean fail = true;
                ArrayList<String> loadedThumbNames = new ArrayList<String>();
                try {
                    long startTime = System.nanoTime();
                    while (thumbFileReaded < thumbFileLength && thumbFileReadedOk < thumbFileLength) {
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int pathLen = thumbFile.readInt();
                        if (pathLen <= 0 || pathLen > 1024 * 1014) {
                            break;
                        }
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        ByteBuffer pathBB = ByteBuffer.allocate(pathLen);
                        if (thumbFileReaded + pathLen > thumbFileLength) {
                            break;
                        }
                        thumbFile.read(pathBB.array());
                        if (pathBB.array().length == 0) {
                            break;
                        }
                        thumbFileReaded += pathLen;
                        if (thumbFileReaded + Double.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        double ratio = thumbFile.readDouble();
                        thumbFileReaded += Double.SIZE / Byte.SIZE;
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int color = thumbFile.readInt();
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int thumbW = thumbFile.readInt();
                        if (thumbW <= 0 || thumbW > 1024) {
                            break;
                        }
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int thumbH = thumbFile.readInt();
                        if (thumbH <= 0 || thumbH > 1024) {
                            break;
                        }
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int thumbLen = thumbFile.readInt();
                        if (thumbLen <= 0 || thumbLen > 1024 * 1014) {
                            break;
                        }
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        if (thumbFileReaded + thumbLen > thumbFileLength) {
                            break;
                        }
                        ByteBuffer thumbBB = ByteBuffer.allocate(thumbLen);
                        thumbFile.read(thumbBB.array());
                        thumbFileReaded += thumbLen;
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int featuresSize = thumbFile.readInt();
                        if (featuresSize <= 0 || featuresSize > 1024 * 1024 * 16) {
                            break;
                        }
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        if (thumbFileReaded + featuresSize > thumbFileLength) {
                            break;
                        }
                        ByteBuffer featuresBB = ByteBuffer.allocate(featuresSize);
                        thumbFile.read(featuresBB.array());
                        thumbFileReaded += featuresSize;
                        if (thumbFileReaded + Integer.SIZE / Byte.SIZE > thumbFileLength) {
                            break;
                        }
                        int objectsSize = thumbFile.readInt();
                        if (objectsSize <= 0 || objectsSize > 1024 * 1024 * 16) {
                            break;
                        }
                        thumbFileReaded += Integer.SIZE / Byte.SIZE;
                        if (thumbFileReaded + objectsSize > thumbFileLength) {
                            break;
                        }
                        ByteBuffer objectsBB = ByteBuffer.allocate(objectsSize);
                        thumbFile.read(objectsBB.array());
                        thumbFileReaded += objectsSize;
                        Features features = new Features();
                        features.setGlobalFeaturesFromRawArray(featuresBB.array());
                        Objects objects = new Objects();
                        objects.setFromRawArray(objectsBB.array());
                        String thumbName = new String(pathBB.array(), Charset.forName("UTF-8"));
                        try {
                            thumbs.put(thumbName, new Thumb(new Image(new ByteArrayInputStream(thumbBB.array()), thumbName, false), ratio, color, features, objects));
                            loadedThumbNames.add(thumbName);
                        } catch (SlickException ee) {
                            ee.printStackTrace();
                            System.out.println(ee);
                        }
                        thumbFileReadedOk = thumbFileReaded;
                        if (System.nanoTime() - startTime > 50000000) {
                            fail = false;
                            break;
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                try {
                    if (fail) {
                        thumbFile.setLength(thumbFileReadedOk);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }
                tree.walk(false, new TreeProcessor<Frame>() {
                    @Override
                    public void processNode(TreeNode<Frame> node) {
                        Frame frame = node.getData();
                        if (frame.isLeaf()) {
                            if (loadedThumbNames.contains(frame.getThumbName())) {
                                frame.setRatio(thumbs.get(frame.getThumbName()).getRatio());
                                frame.setThumb(thumbs.get(frame.getThumbName()));
                                drawPos = true;
                                repaint = true;
                            }
                        }
                    }
                });
                if (thumbFileReadedOk >= thumbFileLength || fail) {
                    thumbsLoading = false;
                    tree.walk(false, new TreeProcessor<Frame>() {
                        @Override
                        public void processNode(TreeNode<Frame> node) {
                            Frame frame = node.getData();
                            if (frame.isLeaf()) {
                                if (thumbs.containsKey(frame.getThumbName())) {
                                    frame.setRatio(thumbs.get(frame.getThumbName()).getRatio());
                                    frame.setThumb(thumbs.get(frame.getThumbName()));
                                } else {
                                    HashMap<String,ArrayList<String>> imageGTPaths = new HashMap<String,ArrayList<String>>();
                                    for (String gtClassName : frame.getGTPaths().keySet()) {
                                        imageGTPaths.put(gtClassName,new ArrayList<String>());
                                        for (String gtPath : frame.getGTPaths().get(gtClassName)) {
                                            imageGTPaths.get(gtClassName).add(rootDirPath + File.separator + gtPath);
                                        }
                                    }
                                    synchronized (picturesLock) {
                                        picturesToProces.add(new PictureInternal(rootDirPath + File.separator + frame.getPath(), imageGTPaths, frame, thumbSize));
                                    }
                                }
                            }
                        }
                    });
                    drawPos = true;
                    repaint = true;
                }
            }
            long startTime = System.nanoTime();
            while (true)
            {
                PictureInternal pict = null;
                synchronized (picturesLock) {
                    if (picturesReady.size() > 0) {
                        pict = picturesReady.get(0);
                        picturesReady.remove(0);
                    }
                }
                if (pict != null) {
                    Frame frame = pict.getFrame();
                    Image thumbImage = null;
                    try {
                        thumbImage = new Image(new ByteArrayInputStream(pict.getThumbData()), frame.getThumbName(), false);
                    } catch (SlickException ee) {
                        ee.printStackTrace();
                        System.out.println(ee);
                    }
                    if (thumbImage != null) {
                        frame.setRatio(pict.getRatio());
                        Thumb thumb = new Thumb(thumbImage, frame.getRatio(), pict.getColor(), pict.getFeatures(), pict.getObjects());
                        thumbs.put(frame.getThumbName(), thumb);
                        frame.setThumb(thumb);
                        byte[] pathBB = frame.getThumbName().getBytes(Charset.forName("UTF-8"));
                        ByteArrayOutputStream thumbBB = new ByteArrayOutputStream(0);
                        ImageOut.write(thumbImage, "PNG", thumbBB);
                        if (pathBB.length > 0 && thumbBB.size() > 0) {
                            long lengthOk = 0;
                            try {
                                RandomAccessFile thumbFile = new RandomAccessFile(rootDirPath + File.separator + ".thumb.db", "rw");
                                lengthOk = thumbFile.length();
                                thumbFile.seek(lengthOk);
                                thumbFile.writeInt(pathBB.length);
                                thumbFile.write(pathBB);
                                thumbFile.writeDouble(thumb.getRatio());
                                thumbFile.writeInt(thumb.getColor());
                                thumbFile.writeInt(thumbImage.getWidth());
                                thumbFile.writeInt(thumbImage.getHeight());
                                thumbFile.writeInt(thumbBB.size());
                                thumbFile.write(thumbBB.toByteArray());
                                thumbFile.writeInt(thumb.getFeatures().getGlobalFeaturesRawArraySize());
                                thumbFile.write(thumb.getFeatures().getGlobalFeaturesAsRawArray());
                                thumbFile.writeInt(thumb.getObjects().getRawArraySize());
                                thumbFile.write(thumb.getObjects().getAsRawArray());
                            } catch (IOException e) {
                                e.printStackTrace();
                                System.out.println(e);
                                try {
                                    if (lengthOk > 0) {
                                        RandomAccessFile thumbFile = new RandomAccessFile(rootDirPath + File.separator + ".thumb.db", "rw");
                                        if (thumbFile.length() > lengthOk) {
                                            thumbFile.setLength(lengthOk);
                                        }
                                    }
                                } catch (IOException ee) {
                                    ee.printStackTrace();
                                    System.out.println(ee);
                                }
                            }
                        }
                    }
                }
                ImageInternal imageInternal = null;
                synchronized (imagesLock) {
                    if (imagesReady.size() > 0) {
                        imageInternal = imagesReady.get(0);
                        imagesReady.remove(0);
                    }
                }
                if (imageInternal != null) {
                    Frame frame = imageInternal.getFrame();
                    Image imageFull = null;
                    try {
                        imageFull = new Image(new ByteArrayInputStream(imageInternal.getImageData()), frame.getThumbName() + "_FULL", false);
                    } catch (SlickException ee) {
                        ee.printStackTrace();
                        System.out.println(ee);
                    }
                    imageInternal.setImageData(null);
                    if (imageFull != null) {
                        frame.getThumb().setImageFull(imageFull);
                    }
                }
                if (pict != null || imageInternal != null) {
                    drawPos = true;
                    repaint = true;
                }
                if (System.nanoTime() - startTime > 25000000 || (pict == null && imageInternal == null)) {
                    break;
                }
            }
            if (repos) {
                tree.walk(false, new TreeProcessor<Frame>() {
                    @Override
                    public void processNode(TreeNode<Frame> node) {
                        Frame frame = node.getData();
                        if (node == tree.getRoot()) {
                            frame.setX(0.0);
                            frame.setY(0.0);
                        } else {
                            Frame parentFrame = node.getParent().getData();
                            frame.setX(parentFrame.getX() + frame.getRX());
                            frame.setY(parentFrame.getY() + frame.getRY());
                        }
                    }
                });
                drawPos = true;
            }
            if (fitView) {
                class MyDouble {
                    public double value = 0.0;
                }
                final MyDouble minX = new MyDouble();
                final MyDouble minY = new MyDouble();
                final MyDouble maxX = new MyDouble();
                final MyDouble maxY = new MyDouble();
                tree.walk(false, new TreeProcessor<Frame>() {
                    @Override
                    public void processNode(TreeNode<Frame> node) {
                        Frame frame = node.getData();
                        minX.value = Math.min(minX.value, frame.getX() - frame.getR());
                        minY.value = Math.min(minY.value, frame.getY() - frame.getR());
                        maxX.value = Math.max(maxX.value, frame.getX() + frame.getR());
                        maxY.value = Math.max(maxY.value, frame.getY() + frame.getR());
                    }
                });
                double x = (minX.value + maxX.value) / 2.0;
                double y = (minY.value + maxY.value) / 2.0;
                double dx = maxX.value - x;
                double dy = maxY.value - y;
                double r = Math.max(dx, dy);
                double sr = Math.min(viewW - 25, viewH - 25) / 2.0;
                viewX = x;
                viewY = y;
                viewScale = r > 0.0 ? sr / r : 1.0;
                fitView = false;
                drawPos = true;
            }
            if (drawPos) {
                tree.walk(false, new TreeProcessor<Frame>() {
                    @Override
                    public void processNode(TreeNode<Frame> node) {
                        Frame frame = node.getData();
                        double px = (frame.getX() - viewX) * viewScale + viewW / 2.0;
                        double py = (frame.getY() - viewY) * viewScale + viewH / 2.0;
                        double pr = frame.getR() * viewScale;
                        double ps = frame.getS() * viewScale;
                        frame.setPX(px);
                        frame.setPY(py);
                        frame.setPR(pr);
                        frame.setPS(ps);
                        if (frame.isLeaf()) {
                            double ratio = frame.getRatio();
                            if (ratio < eps) {
                                ratio = eps;
                            }
                            double pw = Math.sqrt((pr * pr * ratio *ratio * 4.0) / (ratio * ratio +  1.0));
                            double ph = pw / ratio;
                            frame.setPW(pw);
                            frame.setPH(ph);
                        }
                    }
                });
                drawPos = false;
            }
        }
    }

    @Override
    public void render(GameContainer gc, Graphics g) throws SlickException {
        long renderStartTime = System.nanoTime();
        g.setBackground(new Color(UIManager.getColor("Panel.background").getRGB()));
        g.clear();
        if (tree == null) {
            return;
        }
        if (!repaint) {
            return;
        }
        synchronized (treeLock) {
            ArrayList<Frame> framesToDraw = new ArrayList<Frame>();
            tree.walk(false, new TreeProcessor<Frame>() {
                @Override
                public void processNode(TreeNode<Frame> node) {
                    Frame frame = node.getData();
                    double x = frame.getPX();
                    double y = frame.getPY();
                    double r = frame.getPR();
                    if (x + r >= 0.0 && x - r < viewW && y + r >= 0.0 && y - r < viewH) {
                        double s = frame.getPS();
                        if (frame.isLeaf()) {
                            framesToDraw.add(frame);
                            /*
                            //g.setColor(new Color(0,255,0));
                            //g.fillRect((float)(x - r), (float)(y - r), (float)(r * 2.0), (float)(r * 2.0));
                            //g.setColor(new Color(0,0,0));
                            //g.drawOval((float)(x - r), (float)(y - r), (float)(r * 2.0), (float)(r * 2.0));
                            //g.setColor(new Color(0,255,0));
                            //g.fillRect((float)(x - s / 2.0), (float)(y - s / 2.0), (float)(s), (float)(s));
                            boolean drawStub = true;
                            Thumb thumb = frame.getThumb();
                            if (thumb != null) {
                                Image thumbImage = thumb.getImage();
                                if (thumbImage != null) {
                                    double w = frame.getPW();
                                    double h = frame.getPH();
                                    if (w >= 6 || h >= 6) {
                                        g.drawImage(thumbImage, (float) (x - w / 2.0), (float) (y - h / 2.0), (float) (x + w / 2.0), (float) (y + h / 2.0), (float) (0.0), (float) (0.0), (float) (thumbImage.getWidth() - 1.0), (float) (thumbImage.getHeight() - 1.0));
                                    } else {
                                        g.setColor(new Color(thumb.getColor()));
                                        g.fillRect((float) (x - w / 2.0), (float) (y - h / 2.0), (float) (w), (float) (h));
                                    }
                                    g.setColor(new Color(0, 0, 0));
                                    g.drawRect((float) (x - w / 2.0), (float) (y - h / 2.0), (float) (w), (float) (h));
                                    drawStub = false;
                                }
                            }
                            if (drawStub) {
                                double ss = s * 0.9;
                                g.setColor(new Color(0, 0, 0));
                                g.drawRect((float) (x - ss / 2.0), (float) (y - ss / 2.0), (float) (ss), (float) (ss));
                                g.drawLine((float) (x - ss / 2.0), (float) (y - ss / 2.0), (float) (x + ss / 2.0), (float) (y + ss / 2.0));
                                g.drawLine((float) (x - ss / 2.0), (float) (y + ss / 2.0), (float) (x + ss / 2.0), (float) (y - ss / 2.0));
                            }
                            */
                        } else {
                            g.setColor(new Color(199, 221, 236)); //102,163,203
                            //g.fillOval((float)(x - r), (float)(y - r), (float)(r * 2.0), (float)(r * 2.0));
                            g.setColor(new Color(49, 130, 186));
                            g.setLineWidth(3.0f);
                            g.drawOval((float) (x - r), (float) (y - r), (float) (r * 2.0), (float) (r * 2.0));
                        }
                    } else {
                        Thumb thumb = frame.getThumb();
                        if (thumb != null) {
                            if (thumb.getImageFull() != null) {
                                thumb.setImageFullLoadRequestTime(0);
                                try {
                                    thumb.getImageFull().destroy();
                                } catch (Exception e) {}
                                thumb.setImageFull(null);
                            }
                        }
                    }
                }
            });
            Collections.sort(framesToDraw, new Comparator<Frame>() {
                public int compare(Frame frame1, Frame frame2) {
                    double x1 = frame1.getPX() - viewW / 2.0;
                    double y1 = frame1.getPY() - viewH / 2.0;
                    double x2 = frame2.getPX() - viewW / 2.0;
                    double y2 = frame2.getPY() - viewH / 2.0;
                    double d1 = Math.sqrt(x1 * x1 + y1 * y1);
                    double d2 = Math.sqrt(x2 * x2 + y2 * y2);
                    return (d1 == d2) ? 0 : (d1 < d2) ? -1 : 1;
                }
            });
            int thumbsFullCnt = 0;
            int thumbsCnt = 0;
            for (Frame frame : framesToDraw) {
                double x = frame.getPX();
                double y = frame.getPY();
                double r = frame.getPR();
                double s = frame.getPS();
                double w = frame.getPW();
                double h = frame.getPH();
                boolean drawStub = true;
                Thumb thumb = frame.getThumb();
                if (thumb != null) {
                    Image thumbImage = null;
                    if (Math.min(w, h) >= 128 && thumbsFullCnt < 100) {
                        thumbImage = thumb.getImageFull();
                        if (thumbImage == null && (renderStartTime == 0 || renderStartTime - thumb.getImageFullLoadRequestTime() > 10000000000L)) {
                            thumb.setImageFullLoadRequestTime(renderStartTime);
                            synchronized (imagesLock) {
                                imagesToProces.add(new ImageInternal(rootDirPath + File.separator + frame.getPath(), frame));
                            }
                        }
                        thumbsFullCnt ++;
                    } else {
                        thumb.setImageFullLoadRequestTime(0);
                        try {
                            thumb.getImageFull().destroy();
                        } catch (Exception e) {}
                        thumb.setImageFull(null);
                    }
                    if (thumbImage == null) {
                        thumbImage = thumb.getImage();
                    }
                    if (thumbImage != null) {
                        if (Math.min(w, h) >= 6 && thumbsCnt < 5000) {
                            g.drawImage(thumbImage, (float) (x - w / 2.0), (float) (y - h / 2.0), (float) (x + w / 2.0), (float) (y + h / 2.0), (float) (0.0), (float) (0.0), (float) (thumbImage.getWidth() - 1.0), (float) (thumbImage.getHeight() - 1.0));
                            thumbsCnt ++;
                        } else {
                            g.setColor(new Color(thumb.getColor()));
                            g.fillRect((float) (x - w / 2.0), (float) (y - h / 2.0), (float) (w), (float) (h));
                        }
                        g.setColor(new Color(0, 0, 0));
                        g.setLineWidth(1.0f);
                        g.drawRect((float) (x - w / 2.0), (float) (y - h / 2.0), (float) (w), (float) (h));
                        for (int objectClassId : thumb.getObjects().getObjectClasses()) {
                            for (ObjectPos objectPos : thumb.getObjects().getObjects(objectClassId)) {
                                Rectangle2D.Double rect = objectPos.getRect();
                                g.setColor(new Color(255, 0, 255));
                                float lw = (float)Math.min(w, h) / 10.0f;
                                lw = lw > 5.0f ? 5.0f : lw;
                                g.setLineWidth(lw);
                                g.drawRect((float) (x + rect.getX() * w - w / 2.0), (float) (y + rect.getY() * h - h / 2.0), (float) (rect.getWidth() * w), (float) (rect.getHeight() * h));
                            }
                        }
                        drawStub = false;
                    }
                }
                if (drawStub) {
                    double ss = s * 0.9;
                    g.setColor(new Color(180, 180, 180));
                    g.fillRect((float) (x - ss / 2.0), (float) (y - ss / 2.0), (float) (ss), (float) (ss));
                    g.setColor(new Color(0, 0, 0));
                    g.setLineWidth(1.0f);
                    g.drawRect((float) (x - ss / 2.0), (float) (y - ss / 2.0), (float) (ss), (float) (ss));
                    /*
                    g.drawLine((float) (x - ss / 2.0), (float) (y - ss / 2.0), (float) (x + ss / 2.0), (float) (y + ss / 2.0));
                    g.drawLine((float) (x - ss / 2.0), (float) (y + ss / 2.0), (float) (x + ss / 2.0), (float) (y - ss / 2.0));
                    */
                }
            }
            //repaint = false;
        }
        //g.drawString(String.format("Howdy! %d", cnt++), 100, 100);
    }

    public void setTree(Tree<Frame> tree) {
        synchronized (treeLock) {
            this.tree = tree;
            rootDirPath = tree.getRoot().getData().getPath();
            thumbs.clear();
            loadThumbs = true;
            fitView = true;
            repaint = true;
            thumbsLoading = false;
            thumbFile = null;
        }
    }

    private static float[] double2float(double[] array) {
        float[] res = new float[array.length];
        for (int i = 0; i < array.length; i ++) {
            res[i] = (float)array[i];
        }
        return res;
    }
}
