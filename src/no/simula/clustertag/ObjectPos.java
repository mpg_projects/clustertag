package no.simula.clustertag;

import java.awt.geom.Rectangle2D;

public class ObjectPos {

    private double value = 0.0;
    private Rectangle2D.Double rect = new Rectangle2D.Double();

    public ObjectPos() { }

    public ObjectPos(double value, Rectangle2D.Double rect) {
        this.value = value;
        this.rect = rect;
    }

    public double getValue() { return this.value; }

    public void setValue(double value) { this.value = value; }

    public Rectangle2D.Double getRect() { return this.rect; }

    public void setRect(Rectangle2D.Double rect) { this.rect = rect; }
}

