package no.simula.clustertag;

import java.util.*;

public class Tree<T> {

    private TreeNode<T> root;

    public Tree() {
        super();
    }

    public TreeNode<T> getRoot() {
        return this.root;
    }

    public void setRoot(TreeNode<T> root) {
        this.root = root;
    }

    public int getNumberOfNodes() {
        int numberOfNodes = 0;

        if(root != null) {
            numberOfNodes = getNumberOfNodesInternal(root) + 1;
        }

        return numberOfNodes;
    }

    private int getNumberOfNodesInternal(TreeNode<T> node) {
        int numberOfNodes = node.getNumberOfChildren();

        for(TreeNode<T> child : node.getChildren()) {
            numberOfNodes += getNumberOfNodesInternal(child);
        }

        return numberOfNodes;
    }

    public boolean exists(T dataToFind) {
        return (find(dataToFind) != null);
    }

    public TreeNode<T> find(T dataToFind) {
        TreeNode<T> returnNode = null;

        if(root != null) {
            returnNode = findInternal(root, dataToFind);
        }

        return returnNode;
    }

    private TreeNode<T> findInternal(TreeNode<T> currentNode, T dataToFind) {
        TreeNode<T> returnNode = null;
        int i = 0;

        if (currentNode.getData().equals(dataToFind)) {
            returnNode = currentNode;
        }

        else if(currentNode.hasChildren()) {
            i = 0;
            while(returnNode == null && i < currentNode.getNumberOfChildren()) {
                returnNode = findInternal(currentNode.getChildAt(i), dataToFind);
                i++;
            }
        }

        return returnNode;
    }

    public boolean isEmpty() {
        return (root == null);
    }

    public List<TreeNode<T>> build(boolean childrenFirst) {
        List<TreeNode<T>> returnList = null;

        if(root != null) {
            returnList = build(root, childrenFirst);
        }

        return returnList;
    }

    public List<TreeNode<T>> build(TreeNode<T> node, boolean childrenFirst) {
        List<TreeNode<T>> traversalResult = new ArrayList<TreeNode<T>>();

        buildInternal(node, traversalResult, childrenFirst);

        return traversalResult;
    }

    private void buildInternal(TreeNode<T> node, List<TreeNode<T>> traversalResult, boolean childrenFirst) {
        if (!childrenFirst) {
            traversalResult.add(node);
        }

        for(TreeNode<T> child : node.getChildren()) {
            buildInternal(child, traversalResult, childrenFirst);
        }

        if (childrenFirst) {
            traversalResult.add(node);
        }
    }

    public Map<TreeNode<T>, Integer> buildWithDepth(boolean childrenFirst) {
        Map<TreeNode<T>, Integer> returnMap = null;

        if(root != null) {
            returnMap = buildWithDepth(root, childrenFirst);
        }

        return returnMap;
    }

    public Map<TreeNode<T>, Integer> buildWithDepth(TreeNode<T> node, boolean childrenFirst) {
        Map<TreeNode<T>, Integer> traversalResult = new LinkedHashMap<TreeNode<T>, Integer>();

        buildWithDepthInternal(node, traversalResult, 0, childrenFirst);

        return traversalResult;
    }

    private void buildWithDepthInternal(TreeNode<T> node, Map<TreeNode<T>, Integer> traversalResult, int depth, boolean childrenFirst) {
        if (!childrenFirst) {
            traversalResult.put(node, depth);
        }

        for(TreeNode<T> child : node.getChildren()) {
            buildWithDepthInternal(child, traversalResult, depth + 1, childrenFirst);
        }

        if (childrenFirst) {
            traversalResult.put(node, depth);
        }
    }

    public void walk(boolean childrenFirst, TreeProcessor<T> processor) {
        if (root != null) {
            walk(root, childrenFirst, processor);
        }
    }

    public void walk(TreeNode<T> node, boolean childrenFirst, TreeProcessor<T> processor) {
        if (!childrenFirst) {
            processor.processNode(node);
        }

        for(TreeNode<T> child : node.getChildren()) {
            walk(child, childrenFirst, processor);
        }

        if (childrenFirst) {
            processor.processNode(node);
        }
    }

    public String toString() {

        String stringRepresentation = "";

        if(root != null) {
            stringRepresentation = build(false).toString();

        }

        return stringRepresentation;
    }

    public String toStringWithDepth() {

        String stringRepresentation = "";

        if(root != null) {
            stringRepresentation = buildWithDepth(false).toString();
        }

        return stringRepresentation;
    }
}
