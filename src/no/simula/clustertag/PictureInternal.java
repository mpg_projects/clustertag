package no.simula.clustertag;

import java.util.ArrayList;
import java.util.HashMap;

public class PictureInternal {

    private final String imagePath;
    HashMap<String,ArrayList<String>> imageGTPaths;
    private final Frame frame;
    private final int thumbSize;
    private double ratio = 1.0;
    private int color = 0;
    private byte[] thumbData;
    private Features features = new Features();
    private Objects objects = new Objects();

    public PictureInternal(String imagePath, HashMap<String,ArrayList<String>> imageGTPaths, Frame frame, int thumbSize) {
        super();
        this.imagePath = imagePath;
        this.imageGTPaths = imageGTPaths;
        this.frame = frame;
        this.thumbSize = thumbSize;
    }

    public String getImagePath() { return this.imagePath; }

    public HashMap<String,ArrayList<String>> getImageGTPaths() { return this.imageGTPaths; }

    public Frame getFrame() { return this.frame; }

    public int getThumbSize() {
        return this.thumbSize;
    }

    public double getRatio() {
        return this.ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public int getColor() { return this.color; }

    public void setColor(int color) {
        this.color = color;
    }

    public byte[] getThumbData() { return this.thumbData; }

    public void setThumbData(byte[] thumbData) {
        this.thumbData = thumbData;
    }

    public Features getFeatures() { return this.features; }

    public void setFeatures(Features features) {
        this.features = features;
    }

    public Objects getObjects() { return this.objects; }

    public void setObjects(Objects objects) {
        this.objects = objects;
    }
}

