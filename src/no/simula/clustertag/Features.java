package no.simula.clustertag;

import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;

public class Features {

    private static final String globalFeatureClassNamesPrefix = "net.semanticmetadata.lire.imageanalysis.features.global.";
    private static final String[] globalFeatureClassNames = {
            "LocalBinaryPatterns",
            "FCTH",
            "AutoColorCorrelogram",
            "PHOG",
            "CEDD",
            "JpegCoefficientHistogram",
            "BinaryPatternsPyramid",
            "Tamura",
            "LuminanceLayout",
            "FuzzyOpponentHistogram",
            "RotationInvariantLocalBinaryPatterns",
            "ScalableColor",
            "Gabor",
            "OpponentHistogram",
            "SimpleColorHistogram",
            "EdgeHistogram",
            "FuzzyColorHistogram",
            "ColorLayout",
            "JCD",
            "centrist.SimpleCentrist",
            "centrist.SpatialPyramidCentrist",
            "spatialpyramid.SPLBP",
            "spatialpyramid.SPACC",
            "spatialpyramid.SPFCTH",
            "spatialpyramid.SPJCD",
            "spatialpyramid.SPCEDD",
            "joint.RankAndOpponent",
            "joint.JointHistogram",
            "joint.LocalBinaryPatternsAndOpponent",
    };

    private static class GlobalFeatureInfo
    {
        public String className;
        public String name;
        public int vectorSize = 0;
        public Class<? extends GlobalFeature> extractor;
        public GlobalFeatureInfo(String className, String name, int vectorSize, Class<? extends GlobalFeature> extractor) {
            this.className = className;
            this.name = name;
            this.vectorSize = vectorSize;
            this.extractor = extractor;
        }
    }

    private static ArrayList<GlobalFeatureInfo> globalFeatureInfos = new ArrayList<GlobalFeatureInfo>();
    private static int staticMembersInitHelper = initStaticMembers();

    private static int initStaticMembers() {
        for (String globalFeatureClassName : globalFeatureClassNames) {
            try {
                GlobalFeature featureExtractor = (GlobalFeature) Class.forName(globalFeatureClassNamesPrefix + globalFeatureClassName).newInstance();
                BufferedImage img = new BufferedImage(128, 128, BufferedImage.TYPE_3BYTE_BGR);
                featureExtractor.extract(img);
                int featureVectorSize = featureExtractor.getFeatureVector().length;
                globalFeatureInfos.add(new GlobalFeatureInfo(globalFeatureClassName, featureExtractor.getFeatureName(), featureVectorSize, featureExtractor.getClass()));
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
                System.exit(1);
            }
        }
        return 1;
    }


    public static int getGlobalFeaturesNum() {
        return globalFeatureInfos.size();
    }

    public static String getGlobalFeatureClassName(int idx) {
        String res = new String();
        if (idx >= 0 || idx < globalFeatureInfos.size()) {
            res = globalFeatureInfos.get(idx).className;
        }
        return res;
    }

    public static String getGlobalFeatureName(int idx) {
        String res = new String();
        if (idx >= 0 || idx < globalFeatureInfos.size()) {
            res = globalFeatureInfos.get(idx).name;
        }
        return res;
    }

    public static int getGlobalFeatureVectorSize(int idx) {
        int res = 0;
        if (idx >= 0 || idx < globalFeatureInfos.size()) {
            res = globalFeatureInfos.get(idx).vectorSize;
        }
        return res;
    }

    public static ArrayList<float[]> getGlobalFeaturesZeroSet() {
        ArrayList<float[]> res = new ArrayList<float[]>();
        for (GlobalFeatureInfo globalFeatureInfo : globalFeatureInfos) {
            res.add(new float [globalFeatureInfo.vectorSize]);
        }
        return res;
    }

    public static float[] getGlobalFeatureZeroVector(int idx) {
        float[] res = null;
        if (idx >= 0 || idx < globalFeatureInfos.size()) {
            res = new float[globalFeatureInfos.get(idx).vectorSize];
        }
        return res;
    }

    public static ArrayList<GlobalFeature> getGlobalFeatureExtractorInstancesSet() {
        ArrayList<GlobalFeature> res = new ArrayList<GlobalFeature>();
        for (GlobalFeatureInfo globalFeatureInfo : globalFeatureInfos) {
            try {
                res.add(globalFeatureInfo.extractor.newInstance());
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
                System.exit(1);
            }
        }
        return res;
    }

    public static GlobalFeature getGlobalFeatureExtractorInstance(int idx) {
        GlobalFeature res = null;
        if (idx >= 0 || idx < globalFeatureInfos.size()) {
            try {
                res = globalFeatureInfos.get(idx).extractor.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(e);
                System.exit(1);
            }
        }
        return res;
    }

    private ArrayList<float[]> globalFeatures = new ArrayList<float[]>();

    public Features() {
        super();
        globalFeatures = getGlobalFeaturesZeroSet();
    }

    public ArrayList<float[]> getGlobalFeatures() { return this.globalFeatures; }

    public void setGlobalFeatures(ArrayList<float[]> globalFeatures) { this.globalFeatures = globalFeatures; }

    public float[] getGlobalFeature(int idx) {
        float[] res = null;
        if (idx >= 0 || idx < globalFeatures.size()) {
            res = globalFeatures.get(idx);
        }
        return res;
    }

    public void setGlobalFeature(int idx, float[] featureVector) {
        if (idx >= 0 || idx < globalFeatures.size()) {
            globalFeatures.set(idx, featureVector);
        }
    }

    public int getGlobalFeaturesRawArraySize() {
        int size = 0;
        for (int idx = 0; idx < globalFeatures.size(); idx++) {
            size += Float.SIZE * globalFeatures.get(idx).length / Byte.SIZE;
        }
        return size;
    }

    public byte[] getGlobalFeaturesAsRawArray() {
        byte[] res = null;
        try {
            ByteArrayOutputStream rawBuf = new ByteArrayOutputStream(0);
            DataOutputStream stream = new DataOutputStream(rawBuf);
            for (int idx = 0; idx < globalFeatures.size(); idx++) {
                for (int i = 0; i < globalFeatures.get(idx).length; i++) {
                        stream.writeFloat(globalFeatures.get(idx)[i]);
                }
            }
            stream.flush();
            rawBuf.flush();
            if (rawBuf.toByteArray().length != getGlobalFeaturesRawArraySize()) {
                throw new Exception("Features raw array size mismatch");
            }
            res = rawBuf.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(1);
        }
        return res;
    }

    public void setGlobalFeaturesFromRawArray(byte[] array) {
        try {
            if (array.length != getGlobalFeaturesRawArraySize()) {
                throw new Exception("Features raw array size mismatch");
            }
            ByteArrayInputStream rawBuf = new ByteArrayInputStream(array);
            DataInputStream stream = new DataInputStream(rawBuf);
            for (int idx = 0; idx < globalFeatures.size(); idx++) {
                for (int i = 0; i < globalFeatures.get(idx).length; i++) {
                    globalFeatures.get(idx)[i] = stream.readFloat();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(1);
        }
    }
}

