package no.simula.clustertag;

import org.newdawn.slick.*;
import org.newdawn.slick.Image;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;
import java.util.HashMap;

public class Frame {

    private final TreeNode<Frame> node;
    private final boolean leaf;
    private DefaultMutableTreeNode viewNode;
    private String name = "";
    private String path = "";
    private HashMap<String,ArrayList<String>> gtPaths = new HashMap<String,ArrayList<String>>();
    private long length = 0;
    private long time = 0;
    private String thumbName = "";
    private String label = "";
    private String description = "";
    //private Image image;
    private Thumb thumb;
    private double ratio = 1.0;
    private double d = -1.0;
    private boolean rpos = false;
    private double rx = 0.0;
    private double ry = 0.0;
    private double r = 0.5;
    private double s = Math.sqrt(0.125) * 2.0;
    private boolean sel = false;
    private boolean pos = false;
    private double x = 0.0;
    private double y = 0.0;
    private double px = 0.0;
    private double py = 0.0;
    private double pr = 0.0;
    private double ps = 0.0;
    private double pw = 0.0;
    private double ph = 0.0;
    private double ptw = 0.0;
    private double pth = 0.0;

    public Frame(TreeNode<Frame> node, boolean leaf) {
        super();
        this.node = node;
        this.leaf = leaf;
    }

    public static TreeNode<Frame> newTreeNode(boolean leaf) {
        TreeNode<Frame> node = new TreeNode<Frame>();
        Frame frame = new Frame(node, leaf);
        node.setData(frame);
        return node;
    }

    public TreeNode<Frame> getNode() {
        return this.node;
    }

    public DefaultMutableTreeNode getViewNode() {
        return this.viewNode;
    }

    public void setViewNode(DefaultMutableTreeNode viewNode) {
        this.viewNode = viewNode;
    }

    public boolean isLeaf() {
        return this.leaf;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() { return this.path; }

    public void setPath(String path) { this.path = path; }

    public HashMap<String,ArrayList<String>> getGTPaths() { return this.gtPaths; }

    public void setGTPaths(HashMap<String,ArrayList<String>> gtPaths) { this.gtPaths = gtPaths; }

    public long getLength() { return this.length; }

    public void setLength(long length) { this.length = length; }

    public long getTime() { return this.time; }

    public void setTime(long time) { this.time = time; }

    public String getThumbName() {
        return this.thumbName;
    }

    public void setThumbName(String thumbName) {
        this.thumbName = thumbName;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    //public Image getImage() { return this.image; }

    //public void setImage(Image image) { this.image = image; }

    public Thumb getThumb() {
        return this.thumb;
    }

    public void setThumb(Thumb thumb) {
        this.thumb = thumb;
    }

    public double getRatio() {
        return this.ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }

    public double getD() {
        return this.d;
    }

    public void setD(double d) {
        this.d = d;
    }

    public boolean getRPos() {
        return this.rpos;
    }

    public void setRPos(boolean rpos) {
        this.rpos = rpos;
    }

    public double getRX() {
        return this.rx;
    }

    public void setRX(double rx) {
        this.rx = rx;
    }

    public double getRY() {
        return this.ry;
    }

    public void setRY(double ry) {
        this.ry = ry;
    }

    public double getR() {
        return this.r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getS() {
        return this.s;
    }

    public void setS(double s) {
        this.s = s;
    }

    public boolean getSel() {
        return this.sel;
    }

    public void setSel(boolean sel) {
        this.sel = sel;
    }

    public boolean getPos() {
        return this.pos;
    }

    public void setPos(boolean pos) {
        this.pos = pos;
    }

    public double getX() {
        return this.x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return this.y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getPX() {
        return this.px;
    }

    public void setPX(double px) {
        this.px = px;
    }

    public double getPY() {
        return this.py;
    }

    public void setPY(double py) {
        this.py = py;
    }

    public double getPR() {
        return this.pr;
    }

    public void setPR(double pr) {
        this.pr = pr;
    }

    public double getPS() {
        return this.ps;
    }

    public void setPS(double ps) {
        this.ps = ps;
    }

    public double getPW() {
        return this.pw;
    }

    public void setPW(double pw) {
        this.pw = pw;
    }

    public double getPH() {
        return this.ph;
    }

    public void setPH(double ph) {
        this.ph = ph;
    }

    public double getPTW() {
        return this.ptw;
    }

    public void setPTW(double ptw) {
        this.ptw = ptw;
    }

    public double getPTH() {
        return this.pth;
    }

    public void setPTH(double pth) {
        this.pth = pth;
    }

}

