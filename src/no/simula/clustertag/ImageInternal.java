package no.simula.clustertag;

import java.util.ArrayList;
import java.util.HashMap;

public class ImageInternal {

    private final String imagePath;
    private final Frame frame;
    private byte[] imageData;

    public ImageInternal(String imagePath, Frame frame) {
        super();
        this.imagePath = imagePath;
        this.frame = frame;
    }

    public String getImagePath() { return this.imagePath; }

    public Frame getFrame() { return this.frame; }

    public byte[] getImageData() { return this.imageData; }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }
}

