package no.simula.clustertag;

import org.newdawn.slick.*;
import org.newdawn.slick.Image;

public class Thumb {

    private Image image;
    private Image imageFull = null;
    long imageFullLoadRequestTime = 0;
    private double ratio = 1.0;
    private int color = 0;
    private Features features;
    private Objects objects = new Objects();

    public Thumb(Image image, double ratio, int color, Features features, Objects objects) {
        this.image = image;
        this.ratio = ratio;
        this.color = color;
        this.features = features;
        this.objects = objects;
    }

    public Image getImage() { return this.image; }

    public double getRatio() { return this.ratio; }

    public int getColor() { return this.color; }

    public Features getFeatures() { return this.features; }

    public Objects getObjects() { return this.objects; }

    public Image getImageFull() { return this.imageFull; }

    public void setImageFull(Image imageFull) { this.imageFull = imageFull; }

    public long getImageFullLoadRequestTime() { return this.imageFullLoadRequestTime; }

    public void setImageFullLoadRequestTime(long imageFullLoadRequestTime) { this.imageFullLoadRequestTime = imageFullLoadRequestTime; }
}

