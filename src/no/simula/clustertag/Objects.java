package no.simula.clustertag;

import java.awt.geom.Rectangle2D;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Objects {

    private static final String[] objectClassNames = {
            "unknown",
            "polyp",
    };

    public static int getKnownObjectClassesNum() {
        return objectClassNames.length - 1;
    }

    public static String[] getKnownObjectClassNames() { return objectClassNames; }

    public static String getObjectClassName(int objectClassId) {
        String res = objectClassNames[0];
        if (objectClassId > 0 && objectClassId < objectClassNames.length) {
            res = objectClassNames[objectClassId];
        }
        return res;
    }

    public static int getObjectClassId(String objectClassName) {
        int res = 0;
        if (objectClassName.length() > 0) {
            for (int i = 0; i < objectClassNames.length; i++) {
                if (objectClassNames[i].compareTo(objectClassName) == 0) {
                    res = i;
                    break;
                }
            }
            if (res == 0) {
                for (int i = 0; i < objectClassNames.length; i++) {
                    if (objectClassNames[i].compareToIgnoreCase(objectClassName) == 0) {
                        res = i;
                        break;
                    }
                }
            }
        }
        return res;
    }

    private HashMap<Integer,ArrayList<ObjectPos>> objects = new HashMap<Integer,ArrayList<ObjectPos>>();

    public Objects() {
        super();
    }

    public int[] getObjectClasses() {
        int[] res = new int[objects.keySet().size()];
        try {
            Iterator<Integer> iter = objects.keySet().iterator();
            int cnt = 0;
            for (int i = 0; i < objects.keySet().size() && iter.hasNext(); i ++) {
                res[i] = iter.next();
                cnt ++;
            }
            if (cnt != res.length) {
                throw new Exception("Objects classes array building failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(1);
        }
        return res;
    }

    public ArrayList<ObjectPos> getObjects(int objectClassId) {
        ArrayList<ObjectPos> res = new ArrayList<ObjectPos>();
        if (objectClassId > 0 && objectClassId < objectClassNames.length) {
            if (objects.containsKey(objectClassId)) {
                res = objects.get(objectClassId);
            }
        }
        return res;
    }

    public void addObject(int objectClassId, ObjectPos object) {
        if (objectClassId > 0 && objectClassId < objectClassNames.length) {
            if (!objects.containsKey(objectClassId)) {
                objects.put(objectClassId, new ArrayList<ObjectPos>());
           }
           objects.get(objectClassId).add(object);
        }
    }

    public int getRawArraySize() {
        int[] objectClasses = getObjectClasses();
        int size = Integer.SIZE / Byte.SIZE;
        for (int objectClassIndex = 0; objectClassIndex < objectClasses.length; objectClassIndex++) {
            size += (Integer.SIZE * 2 + Double.SIZE * 5 * objects.get(objectClasses[objectClassIndex]).size()) / Byte.SIZE;
        }
        return size;
    }

    public byte[] getAsRawArray() {
        byte[] res = null;
        try {
            ByteArrayOutputStream rawBuf = new ByteArrayOutputStream(0);
            DataOutputStream stream = new DataOutputStream(rawBuf);
            int[] objectClasses = getObjectClasses();
            stream.writeInt(objectClasses.length);
            for (int objectClassIndex = 0; objectClassIndex < objectClasses.length; objectClassIndex++) {
                int objectClassId = objectClasses[objectClassIndex];
                stream.writeInt(objectClassId);
                ArrayList<ObjectPos> objectPoses = objects.get(objectClassId);
                stream.writeInt(objectPoses.size());
                for (int i = 0; i < objectPoses.size(); i++) {
                    stream.writeDouble(objectPoses.get(i).getValue());
                    Rectangle2D.Double rect = objectPoses.get(i).getRect();
                    stream.writeDouble(rect.getX());
                    stream.writeDouble(rect.getY());
                    stream.writeDouble(rect.getWidth());
                    stream.writeDouble(rect.getHeight());
                }
            }
            stream.flush();
            rawBuf.flush();
            if (rawBuf.toByteArray().length != getRawArraySize()) {
                throw new Exception("Objects raw array size mismatch");
            }
            res = rawBuf.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(1);
        }
        return res;
    }

    public void setFromRawArray(byte[] array) {
        try {
            objects.clear();
            ByteArrayInputStream rawBuf = new ByteArrayInputStream(array);
            DataInputStream stream = new DataInputStream(rawBuf);
            int objectClassesNum = stream.readInt();
            if (objectClassesNum < 0) {
                throw new Exception("Objects raw array corrupted in classes data");
            }
            for (int objectClassIndex = 0; objectClassIndex < objectClassesNum; objectClassIndex++) {
                int objectClassId = stream.readInt();
                if (objectClassId < 0 || objectClassId > getKnownObjectClassesNum()) {
                    objectClassId = 0;
                }
                int objectPosesNum = stream.readInt();
                if (objectPosesNum <= 0) {
                    throw new Exception("Objects raw array corrupted in poses data");
                }
                for (int i = 0; i < objectPosesNum; i++) {
                    double value = stream.readDouble();
                    Rectangle2D.Double rect = new Rectangle2D.Double(stream.readDouble(), stream.readDouble(), stream.readDouble(), stream.readDouble());
                    addObject(objectClassId, new ObjectPos(value, rect));
                }
            }
            if (array.length != getRawArraySize()) {
                throw new Exception("Objects raw array size mismatch");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(1);
        }
    }
}

