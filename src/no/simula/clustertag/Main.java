package no.simula.clustertag;

import java.awt.event.ActionListener;

import org.newdawn.slick.*;
import org.newdawn.slick.opengl.PNGDecoder;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.Format;
import java.util.*;

import static java.nio.file.FileVisitResult.CONTINUE;

class Main extends JPanel
        implements ActionListener {

    private static Engine engine;
    private static CanvasGameContainer drawEngineCanvas;

    private int newNodeSuffix = 1;
    private static String OPEN_COMMAND = "open";
    private static String ADD_COMMAND = "add";
    private static String REMOVE_COMMAND = "remove";
    private static String CLEAR_COMMAND = "clear";

    private TreeView treeViewPanel;

    private Tree<Frame> tree;

    final JFileChooser rootDirFileChooser;

    private Path rootDirPath;

    final boolean isMac;
    final boolean isWindows;

    final ArrayList<String> imageFileExtensionList;

    public Main() {
        super(new BorderLayout());

        imageFileExtensionList = new ArrayList<String>(Arrays.asList(new String[]{"jpg", "jpeg", "bmp", "tif", "tiff"}));

        isMac = System.getProperty("os.name").toLowerCase().contains("mac");
        isWindows = System.getProperty("os.name").toLowerCase().contains("windows");

        rootDirFileChooser = new JFileChooser(isWindows ? System.getenv("APPDATA") : System.getProperty("user.home") + File.separator + "Documents");
        rootDirFileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        JButton openButton = new JButton("Select base folder");
        openButton.setActionCommand(OPEN_COMMAND);
        openButton.addActionListener(this);

        JPanel topPanel = new JPanel(new GridLayout(0, 3));
        topPanel.add(openButton);
        add(topPanel, BorderLayout.NORTH);

        treeViewPanel = new TreeView();

        JButton addButton = new JButton("Add");
        addButton.setActionCommand(ADD_COMMAND);
        addButton.addActionListener(this);

        JButton removeButton = new JButton("Remove");
        removeButton.setActionCommand(REMOVE_COMMAND);
        removeButton.addActionListener(this);

        JButton clearButton = new JButton("Clear");
        clearButton.setActionCommand(CLEAR_COMMAND);
        clearButton.addActionListener(this);

        //Lay everything out.
        treeViewPanel.setPreferredSize(new Dimension(300, 150));
        add(treeViewPanel, BorderLayout.CENTER);

        JPanel panel = new JPanel(new GridLayout(0, 3));
        panel.add(addButton);
        panel.add(removeButton);
        panel.add(clearButton);
        add(panel, BorderLayout.SOUTH);
    }

    public void openFolder(String path) {
        try {
            Path rootDirPath = new File(path).toPath().toAbsolutePath();
            String rootDirName = rootDirPath.getFileName().toString();
            Tree<Frame> imagesTree = new Tree<Frame>();
            HashMap<String,TreeNode<Frame>> imageDirNodes = new HashMap<String,TreeNode<Frame>>();
            treeViewPanel.clear();
            treeViewPanel.setRootNodeLabel(rootDirName);
            ArrayList<String> dirsList = new ArrayList<String>();
            ArrayList<String> filesList = new ArrayList<String>();
            Files.walkFileTree(rootDirPath, EnumSet.of(FileVisitOption.FOLLOW_LINKS), Integer.MAX_VALUE,
                    new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                                throws IOException {
                            dirsList.add(dir.toAbsolutePath().toString());
                            /*
                            Path relDirPath = rootDirPath.relativize(dir);
                            String relDir = relDirPath.toString();
                            String relDirName = relDirPath.getFileName().toString();
                            TreeNode<Frame> dirNode = Frame.newTreeNode(false);
                            Frame frame = dirNode.getData();
                            if (relDir.isEmpty()) {
                                frame.setName(rootDirName);
                                frame.setPath(dir.toAbsolutePath().toString());
                                frame.setLabel(rootDirName);
                                imagesTree.setRoot(dirNode);
                            } else {
                                Path parentRelDirPath = rootDirPath.relativize(dir).getParent();
                                String parentRelDir = parentRelDirPath == null ? "" : parentRelDirPath.toString();
                                frame.setName(relDirName);
                                frame.setPath(relDir);
                                frame.setLabel(relDirName);
                                TreeNode<Frame> parentNode = imageDirNodes.get("/" + parentRelDir);
                                parentNode.addChild(dirNode);
                                frame.setViewNode(treeViewPanel.addObject(parentNode.getData().getViewNode(), frame.getLabel()));
                            }
                            imageDirNodes.put("/" + relDir, dirNode);
                            */
                            return CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                                throws IOException {
                            filesList.add(file.toAbsolutePath().toString());
                            /*
                            if (imageFileExtensionList.contains(getFileExtension(file.getFileName().toFile()))) {
                                String fileName = file.getFileName().toString();
                                String parentRelDir = rootDirPath.relativize(file.getParent()).toString();
                                TreeNode<Frame> fileNode = Frame.newTreeNode(true);
                                Frame frame = fileNode.getData();
                                frame.setName(fileName);
                                frame.setPath(rootDirPath.relativize(file).toString());
                                File f = file.toFile();
                                long pathLength = f.length();
                                frame.setLength(pathLength);
                                long pathTime = f.lastModified();
                                frame.setTime(pathTime);
                                frame.setLabel(fileName);
                                frame.setThumbName(fileName + "." + pathLength + "." + pathTime);
                                TreeNode<Frame> parentNode = imageDirNodes.get("/" + parentRelDir);
                                frame.setViewNode(treeViewPanel.addObject(parentNode.getData().getViewNode(), frame.getLabel()));
                                parentNode.addChild(fileNode);
                            }
                            */
                            return CONTINUE;
                        }
                    });
            Comparator<String> FileNameOrder =
                    new Comparator<String>() {
                        public int compare(String str1, String str2) {
                            int i1 = 0;
                            int i2 = 0;
                            while (i1 < str1.length() || i2 < str2.length()) {
                                if (i1 >= str1.length()) {
                                    return -1;
                                }
                                if (i2 >= str2.length()) {
                                    return 1;
                                }
                                char c1 = str1.charAt(i1);
                                char c2 = str2.charAt(i2);
                                if ((c1 >= '0' && c1 <= '9') && (c2 >= '0' && c2 <= '9')) {
                                    String strN1 = "";
                                    String strN2 = "";
                                    boolean N1 = true;
                                    boolean N2 = true;
                                    while (N1 || N2) {
                                        if (N1) {
                                            strN1 += c1;
                                            i1 ++;
                                            if (i1 >= str1.length()) {
                                                return -1;
                                            }
                                            c1 = str1.charAt(i1);
                                            if (!(c1 >= '0' && c1 <= '9')) {
                                                N1 = false;
                                            }
                                        }
                                        if (N2) {
                                            strN2 += c2;
                                            i2 ++;
                                            if (i2 >= str2.length()) {
                                                return 1;
                                            }
                                            c2 = str2.charAt(i2);
                                            if (!(c2 >= '0' && c2 <= '9')) {
                                                N2 = false;
                                            }
                                        }
                                    }
                                    if (strN1.contains(".") || strN2.contains(".")) {
                                        double d1 = Double.valueOf(strN1);
                                        double d2 = Double.valueOf(strN2);
                                        if (d1 != d2) {
                                            return d1 < d2 ? -1 : 1;
                                        }
                                    } else {
                                        long l1 = Long.valueOf(strN1);
                                        long l2 = Long.valueOf(strN2);
                                        if (l1 != l2) {
                                            return l1 < l2 ? -1 : 1;
                                        }
                                    }
                                } else {
                                    if (c1 != c2) {
                                        return c1 < c2 ? -1 : 1;
                                    }
                                    i1++;
                                    i2++;
                                }
                            }
                            return 0;
                        }
                    };
            Collections.sort(dirsList, FileNameOrder);
            Collections.sort(filesList, FileNameOrder);
            for (String dirPathStr : dirsList) {
                Path dir = new File(dirPathStr).toPath();
                Path relDirPath = rootDirPath.relativize(dir);
                String relDir = relDirPath.toString();
                String relDirName = relDirPath.getFileName().toString();
                TreeNode<Frame> dirNode = Frame.newTreeNode(false);
                Frame frame = dirNode.getData();
                if (relDir.isEmpty()) {
                    frame.setName(rootDirName);
                    frame.setPath(dir.toAbsolutePath().toString());
                    frame.setLabel(rootDirName);
                    imagesTree.setRoot(dirNode);
                } else {
                    Path parentRelDirPath = rootDirPath.relativize(dir).getParent();
                    String parentRelDir = parentRelDirPath == null ? "" : parentRelDirPath.toString();
                    frame.setName(relDirName);
                    frame.setPath(relDir);
                    frame.setLabel(relDirName);
                    TreeNode<Frame> parentNode = imageDirNodes.get("/" + parentRelDir);
                    parentNode.addChild(dirNode);
                    frame.setViewNode(treeViewPanel.addObject(parentNode.getData().getViewNode(), frame.getLabel()));
                }
                imageDirNodes.put("/" + relDir, dirNode);
            }
            HashMap<String,HashMap<String,ArrayList<String>>> gtPathsMap = new HashMap<String,HashMap<String,ArrayList<String>>>();
            ArrayList<String> gtPaths = new ArrayList<String>();
            for (String filePathStr : filesList) {
                Path file = new File(filePathStr).toPath();
                if (imageFileExtensionList.contains(getFileExtension(file.getFileName().toFile()))) {
                    String filePath = file.toString();
                    if (filePath.indexOf("_GT[") >= 0 && filePath.indexOf("]", filePath.indexOf("_GT[")) >= 0) {
                        String gtParentPath = filePath.substring(0, filePath.indexOf("_GT[")) + filePath.substring(filePath.indexOf("]", filePath.indexOf("_GT[")) + 1);
                        String gtClassName = filePath.substring(filePath.indexOf("_GT[") + 4, filePath.indexOf("]", filePath.indexOf("_GT[")));
                        if (filesList.contains(gtParentPath)) {
                            if (!gtPathsMap.containsKey(gtParentPath)) {
                                gtPathsMap.put(gtParentPath, new HashMap<String,ArrayList<String>>());
                            }
                            if (!gtPathsMap.get(gtParentPath).containsKey(gtClassName)) {
                                gtPathsMap.get(gtParentPath).put(gtClassName, new ArrayList<String>());
                            }
                            gtPathsMap.get(gtParentPath).get(gtClassName).add(rootDirPath.relativize(file).toString());
                            gtPaths.add(filePath);
                        }
                    }
                }
            }
            for (String filePathStr : filesList) {
                Path file = new File(filePathStr).toPath();
                if (imageFileExtensionList.contains(getFileExtension(file.getFileName().toFile()))) {
                    String fileName = file.getFileName().toString();
                    String filePath = file.toString();
                    if (!gtPaths.contains(filePath)) {
                        String parentRelDir = rootDirPath.relativize(file.getParent()).toString();
                        TreeNode<Frame> fileNode = Frame.newTreeNode(true);
                        Frame frame = fileNode.getData();
                        frame.setName(fileName);
                        frame.setPath(rootDirPath.relativize(file).toString());
                        if (gtPathsMap.containsKey(filePath)) {
                            frame.setGTPaths(gtPathsMap.get(filePath));
                        }
                        File f = file.toFile();
                        long pathLength = f.length();
                        frame.setLength(pathLength);
                        long pathTime = f.lastModified();
                        frame.setTime(pathTime);
                        frame.setLabel(fileName);
                        frame.setThumbName(fileName + "." + pathLength + "." + pathTime);
                        TreeNode<Frame> parentNode = imageDirNodes.get("/" + parentRelDir);
                        frame.setViewNode(treeViewPanel.addObject(parentNode.getData().getViewNode(), frame.getLabel()));
                        parentNode.addChild(fileNode);
                    }
                }
            }
            this.rootDirPath = rootDirPath;
            this.tree = imagesTree;
            engine.setTree(this.tree);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void actionPerformed(ActionEvent actionEvent) {
        String command = actionEvent.getActionCommand();

        if (OPEN_COMMAND.equals(command)) {
            if (rootDirFileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                openFolder(rootDirFileChooser.getSelectedFile().toPath().toString());
            }
        } else if (ADD_COMMAND.equals(command)) {
            treeViewPanel.addObject("New Node " + newNodeSuffix++);
        } else if (REMOVE_COMMAND.equals(command)) {
            treeViewPanel.removeCurrentNode();
        } else if (CLEAR_COMMAND.equals(command)) {
            treeViewPanel.clear();
        }
    }

    private static void createAndShowGUI() {
        JFrame frame = new JFrame("ClusterTag");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Main main = new Main();
        main.setOpaque(true); //content panes must be opaque
        //frame.setContentPane(main);
        frame.add(main, BorderLayout.WEST);
        try{
            engine = new Engine("");
            drawEngineCanvas = new CanvasGameContainer(engine);
            frame.add(drawEngineCanvas, BorderLayout.CENTER);
            drawEngineCanvas.getContainer().setUpdateOnlyWhenVisible(false);
            drawEngineCanvas.getContainer().setAlwaysRender(true);
            drawEngineCanvas.start();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println(e);
        }
        frame.pack();
        frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        frame.setVisible(true);
        main.openFolder("/home/user/Documents/Dataset");
    }

    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
